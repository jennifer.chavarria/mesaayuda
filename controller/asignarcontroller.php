<?php
    //include_once 'model/area.php';
   // include_once 'model/areaobject.php';
    include_once 'model/empleado.php';
    include_once 'model/empleadoobject.php';
   // include_once 'model/asignar.php';
class AsignarController extends Controller {


    function __construct()
    {
        parent ::__construct();
        $this->vista->empleados = [];
        $this->vista->detalles= [];
        $this->vista->mensaje =[];
    }

    function mostrarVista(){
        $empleado = new Empleado();
        $detalle = new Asignar(); 
        session_start();
        if($_SESSION["is_admin"]){
            $this->vista->empleados = $empleado->getAll();
            $this->vista->detalles = $detalle->getAll();
            $this->vista->render('asignar/detalle');
        }else{
            if($_SESSION["is_jefe_area"]){
                $areaId = $_SESSION["area"];
                $this->vista->empleados = $empleado->getEmpleadoPorJefeDeArea($areaId);
                $this->vista->detalles = $detalle->getByAreaAndEstado($areaId);
               
                $this->vista->render('asignar/detalle');
                
            }
        }
    }

    function editarDetalle(){
        $fecha = date('Y-m-d H:i:s');
        $id = $_POST['txtSeleccion'];
        $empleado = $_POST['txtEmpleado'];
        $textotreq = $_POST['txtReq'];
        $fkreq = $this->model->getRequisito($id);
            if($this->model->create([
                'FECHA'=>$fecha,
                'OBSERVACION'=>$textotreq,
                'fkreq'=>$fkreq,
                'ESTADO'=>2,
                'FKEMPLEASIG' => $empleado,
                'IDDETALLEREQ' => $id,
            ])){
                $this->vista->mensaje ="Datos guardados correctamente.";
                $this->mostrarVista();
            }
            else{
                $this->vista->mensaje ="Error al guardar datos."."fk req".$fkreq;
                $this->mostrarVista();
            }

    }
    


}

?>