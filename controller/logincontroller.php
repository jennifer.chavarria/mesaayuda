<?php
    include_once 'model/empleado.php';
    class LoginController extends Controller
    {
        function __construct()
        {
            parent ::__construct();
        }

        function mostrarVista()
        {
            $this->vista->mensaje="";
            $this->vista->render('main/login');
        }

        function login(){
            session_start();
            $empleado = new Empleado();
            $user = isset($_POST['txtUser']) ? $_POST['txtUser'] : null;
            $pass = isset($_POST['txtPass']) ? $_POST['txtPass'] : null;

            $id = $this->model->queryUser($user, $pass);
            if($id){
                $permissions = $this->model->getPermissions($user, $pass);
                $_SESSION["user_id"] = $id;
                $_SESSION["is_admin"] = $permissions[0] == 1;
                $_SESSION["is_jefe_area"] = $this->model->isJefeDeArea($id);
                $_SESSION["area"] = $empleado->getByID($id)->area;
                header("location:../main");
                
            }
            else{
                $this->vista->mensaje="Usuario y/o contraseña incorrectos.";
                $this->vista->render('main/login');
            }
        }

        function logout(){
            session_start();
            $_SESSION = array();
            session_destroy();
            header("location:/mesaayuda");
        }
    }
?>