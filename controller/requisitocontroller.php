<?php 
    include_once 'model/area.php';
    class RequisitoController extends Controller
    {
        function __construct()
        {
            
            parent :: __construct();
        }

        function mostrarVista()
        {
           
        }

        function radicarRequisito(){
            session_start();
            $area = new Area();
            $this->vista->areas = $area->getAreasForRequisito();
            $req = $_POST['txtReq'];
            $area = $_POST['txtArea'];

            if($req && $area){
               $lastid= $this->model->createRequisito($area);
                if($lastid != 0){
                    $fecha = date('Y-m-d H:i:s');
                    $this->model->createDetalleReq([
                        'fecha' => $fecha,
                        'observ' => $req,
                        'fkemple' => $_SESSION["user_id"],
                        'fkreq' => $lastid
                    ]);
                    header("location: ../main?status=1");
                }
                else{
                    header("location: ../main?status=0");
                }
            }
           
            $this->vista->render("main/main");
           
            
        }
    }
?>