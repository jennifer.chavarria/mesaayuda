<?php
include_once 'model/asignar.php';
include_once 'model/estado.php';
include_once 'model/estadoobject.php';
class ResolverController extends Controller {


    function __construct()
    {
        parent ::__construct();
        $this->vista->estados =[];
        $this->vista->detalles=[];
        $this->vista->mensaje =[];
    }

    function mostrarVista(){
        $estado = new Estado();
        $detalle = new Asignar();
        $dato = $detalle->getAllResolver();
        $this->vista->detalles = $dato;
        $this->vista->estados = $estado->getAll();
        $this->vista->render('resolver/resolver');
    }
    
    function editarDetalle(){
        $detalle = new Asignar();
        $fecha = date('Y-m-d H:i:s');
        $id = $_POST['txtSeleccion'];
        $textotreq = $_POST['txtReq'];
        $estado = $_POST['txtEstado'];
        $fkempleasig = $detalle->getEmpleadoAsignado($id);
        $fkreq = $detalle->getRequisito($id); 
        $fkemple= $detalle->getEmpleado($id);        
            if($detalle->createEstado([
                'FECHA'=>$fecha,
                'OBSERVACION'=>$textotreq,
                'fkreq'=>$fkreq,
                'ESTADO'=>$estado,
                'FKEMPLE'=>$fkemple,
                'FKEMPLEASIG'=>$fkempleasig,
                'IDDETALLEREQ'=>$id,
            ])){
                $this->vista->mensaje ="Datos guardados correctamente.";
                $this->mostrarVista();
            }
            else{
                $this->vista->mensaje ="Error al guardar datos.".$fkreq;
                $this->mostrarVista();
            }
    }
}
?>