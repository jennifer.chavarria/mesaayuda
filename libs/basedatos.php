<?php 

 class BaseDatos{

    private $host;
    private $db;
    private $user;
    private $pw;
    private $charset;

    public function __construct()
    {
        $this->host = constant('HOST');
        $this->db = constant('DB');
        $this->user = constant('USER');
        $this->pw = constant('PASS');
        $this->charset = constant('CHARSET');
    }

    function connect()
    {
        try{

            $connection = "mysql:host=" . $this->host . ";dbname=" . $this->db . ";charset=" . $this->charset . "";
            $options = [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_EMULATE_PREPARES => false,
            ];

            $pdo = new PDO($connection, $this->user, $this->pw, $options);

            return $pdo;

        }catch(PDOException $e){
            echo "PDO EXCEPTION " .$e->getMessage();
            print_r('Error de Conexión', $e->getMessage());
        }
        
    }
 }
?>