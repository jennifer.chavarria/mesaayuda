<?php 
    class Controller
    {
        function __construct()
        {
            $this->vista = new View();
        }

        function cargarModelo($modelo)
        {
            $url = 'model/'.$modelo.'.php';
            if(file_exists($url)){
                include_once $url;
                $this->model = new $modelo();
            }
        }
    }
?>