<?php 
    class View
    {
        function __construct()
        {
            
        }

        function render($nombre)
        {
            $archivoVista = 'view/' . $nombre . ".php";

            require $archivoVista;
        }
    }
?>