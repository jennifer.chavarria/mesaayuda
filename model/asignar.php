<?php 
    include_once 'model/detalleobject.php';
    include_once 'model/estado.php';

    class Asignar extends Model{

        function __construct()
        {
            parent ::__construct();
        }


        
        public function getAll(){
            $detalles = [];
            $estadoDao = new Estado();
            try{
                $query = $this->db->connect()->prepare('SELECT * FROM detallereq WHERE FKESTADO >=1 && FKEMPLEASIG IS NULL');
                $query->execute();
                while($row = $query->fetch()){
                    $detalle = new DetalleObject();
                    $detalle->iddetallereq = $row['IDDETALLEREQ'];
                    $detalle->fecha = $row['FECHA'];
                    $detalle->observacion = $row['OBSERVACION'];
                    $detalle->fkemple= $row['FKEMPLE'];
                    if($row['FKESTADO']){
                        $detalle->fkestado = $estadoDao->getEstadoByID($row['FKESTADO'])->nombre;
                    }
                    
                    $detalle->fkempleasig = $row['FKEMPLEASIG'];
                    array_push($detalles, $detalle);
                }
                return $detalles;
            }catch(PDOException $e){

            }
        }

        public function getByAreaAndEstado($id){
            $detalles = [];
            $normalizedId = "";
            if(is_array($id)){
                $normalizedId = $id[0];
            }else{
                $normalizedId = $id;
            }
            
            $estadoDao = new Estado();

            try{
                $query =$this->db->connect()->prepare('SELECT * FROM DETALLEREQ D INNER JOIN REQUISITO R ON D.FKREQ = R.IDREQ WHERE FKESTADO = 1 AND modified IS NULL AND R.FKAREA = '.$id);
                $query->execute();
                while($row = $query->fetch()){
                    $detalle = new DetalleObject();
                    $detalle->iddetallereq = $row['IDDETALLEREQ'];
                    $detalle->fecha = $row['FECHA'];
                    $detalle->observacion = $row['OBSERVACION'];
                    $detalle->fkemple= $row['FKEMPLE'];
                    if($row['FKESTADO']){
                        $detalle->fkestado = $estadoDao->getEstadoByID($row['FKESTADO'])->nombre;
                    }
                    
                    $detalle->fkempleasig = $row['FKEMPLEASIG'];
                    array_push($detalles, $detalle);
                }
               
                
            return $detalles;
            
            }catch(PDOException $e){
                
            }
        }

        
        public function getRequisito($id){
            $requistoFK = '';
            try{
                $query = $this->db->connect()->prepare('SELECT FKREQ FROM detallereq WHERE  IDDETALLEREQ = '.$id); 
                $query->execute();
                while($row = $query->fetch()){
                    $requistoFK = $row['FKREQ'];
                }

                return $requistoFK;
            }catch(PDOException $e){            
                return false;   
            }

        }
       
        public function getEmpleado($id){
            $FKEMPLE = '';
            try{
                $query = $this->db->connect()->prepare('SELECT FKEMPLE FROM detallereq WHERE  IDDETALLEREQ = '.$id); 
                $query->execute();
                while($row = $query->fetch()){
                    $FKEMPLE = $row['FKEMPLE'];
                }

                return $FKEMPLE;
            }catch(PDOException $e){            
                return false;   
            }

        }

        public function getEmpleadoAsignado($id){
            $FKEMPLE = '';
            try{
                $query = $this->db->connect()->prepare('SELECT FKEMPLEASIG FROM detallereq WHERE  IDDETALLEREQ = '.$id); 
                $query->execute();
                while($row = $query->fetch()){
                    $FKEMPLE = $row['FKEMPLEASIG'];
                }

                return $FKEMPLE;
            }catch(PDOException $e){            
                return false;   
            }

        }
        
        public function getAllResolver(){
            $detalles = [];
            try{
                $query = $this->db->connect()->prepare('SELECT * FROM detallereq WHERE FKESTADO>=1 && FKESTADO <= 3 &&  FKEMPLEASIG IS NOT NULL');
                $query->execute();
                while($row = $query->fetch()){
                    $detalle = new DetalleObject();
                    $detalle->iddetallereq = $row['IDDETALLEREQ'];
                    $detalle->fecha = $row['FECHA'];
                    $detalle->observacion = $row['OBSERVACION'];
                    $detalle->fkemple= $row['FKEMPLE'];
                    $detalle->fkestado = $row['FKESTADO'];
                    $detalle->fkempleasig = $row['FKEMPLEASIG'];

                    array_push($detalles, $detalle);
                }
                return $detalles;
            }catch(PDOException $e){

            }
        }
        
        
        public function update($detalle){

            try{
                $query = $this->db->connect()->prepare('UPDATE detallereq SET FKESTADO = :estado, FKEMPLEASIG = :empleado WHERE IDDETALLEREQ = :id ');
                $query->execute([
                    'estado' => $detalle['ESTADO'],
                    'empleado' => $detalle['FKEMPLEASIG'],
                    'id' => $detalle['IDDETALLEREQ']
                ]);

                return true;
            }catch(PDOException $e){            
            
               
                return false;   
            }
        }
        public function create($detalle){

            try{
                $query = $this->db->connect()->prepare('INSERT INTO detallereq (FECHA, OBSERVACION, FKEMPLE, FKREQ, FKESTADO, FKEMPLEASIG) VALUES (:a, :b, :c, :d, :f, :g)');
                $query->execute([
                    'a' =>$detalle['FECHA'],
                    'b'=>$detalle['OBSERVACION'],
                    'c'=>1152211862,
                    'd'=>$detalle['fkreq'],
                    'f' => $detalle['ESTADO'],
                    'g' => $detalle['FKEMPLEASIG'],
                ]);

                $this->updateEstado(0,$detalle['IDDETALLEREQ']);
                $this->updateFilaRadicado($detalle['fkreq']);
                return true;
            }catch(PDOException $e){            
            
                echo $e;
                return false;
            }
        }

        public function updateFilaRadicado($idreq){
            try{
                $query = $this->db->connect()->prepare('UPDATE detallereq SET MODIFIED = 0  WHERE FKREQ = '.$idreq);
                $query->execute();
            }catch(PDOException $e){

            }
        }
        public function createEstado($detalle){

            try{
                $query = $this->db->connect()->prepare('INSERT INTO detallereq (FECHA, OBSERVACION, FKEMPLE, FKREQ, FKESTADO, FKEMPLEASIG) VALUES (:a, :b, :c, :d, :f, :g)');
                $query->execute([
                    'a' =>$detalle['FECHA'],
                    'b'=>$detalle['OBSERVACION'],
                    'c'=>$detalle['FKEMPLE'],
                    'd'=>$detalle['fkreq'],
                    'f' => $detalle['ESTADO'],
                    'g' => $detalle['FKEMPLEASIG'],
                ]);
                 $this->updateEstado(0,$detalle['IDDETALLEREQ']);

                return true;
            }catch(PDOException $e){            
            
                echo $e;
                return false;
            }
        }
        public function updateEstado($estadoparam,$idparam){

            try{
                $query = $this->db->connect()->prepare('UPDATE detallereq SET FKESTADO = :estado  WHERE IDDETALLEREQ = :id ');
                $query->execute([
                    'estado' => $estadoparam,
                    'id' => $idparam
                ]);

                return true;
            }catch(PDOException $e){            
            
               
                return false;
            }
        }
    }

?>