<?php 

    include_once 'model/estadoobject.php';
    class Estado extends Model{

        function __construct()
        {
            parent ::__construct();
        }
   
        public function getAll(){
            $estados = [];
            
            try{
                $query = $this->db->connect()->prepare('SELECT * FROM estado WHERE IDESTADO>=3');
                $query->execute();
                while($row = $query->fetch()){
                    $estado = new EstadoObject();
                    $estado->idestado = $row['IDESTADO'];
                    $estado->nombre = $row['NOMBRE'];
                    array_push($estados, $estado);
                }
                return $estados;
            }catch(PDOException $e){

            }
        }

        public function getEstadoByID($id){
            $estado = new EstadoObject();
            try{
                $query = $this->db->connect()->prepare('SELECT * FROM estado where IDESTADO= '.$id);
                $query->execute();
                while($row = $query->fetch()){
                    $estado->idestado = $row['IDESTADO'];
                    $estado->nombre = $row['NOMBRE'];
                }
                return $estado;
            }catch(PDOException $e){
                echo 'id es '.$id;
                echo '';
                echo 'respuesta'.$e;
            }
        }

    }