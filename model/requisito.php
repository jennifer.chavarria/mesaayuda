<?php 

    class Requisito extends Model{

        function __construct()
        {
            parent ::__construct();
        }


        public function createRequisito($datos)
        {
            $connection = $this->db->connect();
            $query = $connection->prepare('INSERT INTO requisito (FKAREA) VALUES (:a)');

            try{
                $query->execute([
                    'a' => $datos,
                ]);
                $lastid = $connection->lastInsertId();
                return $lastid;
            }catch(PDOException $e){
                return null;
            }
        }

        public function createDetalleReq($datos){
            $query = $this->db->connect()->prepare('INSERT INTO detallereq (FECHA, OBSERVACION, FKEMPLE, FKREQ, FKESTADO, FKEMPLEASIG) VALUES (:a, :b, :c, :d, :e, :f)');

            try{
                $query->execute([
                    'a' => $datos['fecha'],
                    'b' => $datos['observ'],
                    'c' => $datos['fkemple'],
                    'd' => $datos['fkreq'],
                    'e' => 1,
                    'f' => null
                    ]);
            }
            catch(PDOException $e){

            }
        }
    }
?>