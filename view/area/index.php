<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Areas datos</title>
</head>
<body>
    <?php require 'view/static/header.php'?>
    <div class="container">

    <div class="row l6">
    <table width=100%>
                <thead>
                    <tr>
                        <th>id</th>
                        <th>area</th>
                        <th>Encargado de area</th>
                        <th>Editar</th>
                        <th>Borrar</th>
                    </tr>
                </thead>
                <tbody id="tbody-empleados">
                    <?php 
                        include_once 'model/areaobject.php';
                        foreach($this->areas as $registro){
                            $area = new AreaObject();
                            $area = $registro;
                    ?> 
                    <tr id="trow-empleados-<?php echo $area->id;?>">
                        <td><?php echo $area->id;?></td>
                        <td><?php echo $area->nombre;?></td>
                        <td><?php echo $area->encargado;?></td>
                        <td><a href="<?php echo constant('URL');?>area/editarArea/<?php echo $area->id;?>"><i class="material-icons left">edit</i></a></td>
                        <td><a href="<?php echo constant('URL');?>area/borrarArea/<?php echo $area->id;?>"><i class="material-icons left">delete</i></a></td>  
                    </tr>       
                    <?php } ?>
                </tbody>
            </table>
    </div>
    <div class="row l6">
        
        <a href="<?php echo constant('URL');?>area/ingresarArea"class="btn deep-orange accent-3">Ingresar una nueva area</a>
        <?php require 'view/static/footer.php'?>
    </div>
    
    <script src="<?php echo constant('URL');?>resources/js/util.js"></script>
</body>
</html>