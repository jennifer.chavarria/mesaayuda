<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <?php require 'view/static/header.php'?>
    <div class="container">

    <div class="row">
    
    <form class="col s12" method="post" action="<?php echo constant('URL');?>area/guardarArea">
    <div class="row">
        <div class="input-field col l3">
            <input type="number"  id="txtIdArea"  name="txtIdArea" placeholder="Area">
            <label for="area">area</label>
        </div>

        <div class="input-field col l3">
            <input type="text"  id="nombre" name="txtNombre" placeholder="nombre">
            <label for="area">nombre</label>
        </div>
    
</div>
                <div class="input-field col 3">
                        <select name="txtEmpleado">
                            <option value="" disabled selected>Seleccione empleado</option>
                            <?php 
                                include_once 'model/Empleadoobject.php';
                                foreach($this->emple as $registro){
                                    $empleadoobject = new EmpleadoObject();
                                    $empleadoobject = $registro;
                            ?>
                                <option value="<?php echo $empleadoobject->idempleado;?>"><?php echo $empleadoobject->nombre;?></option>
                                <?php } ?>
                        </select>
                        <label>Empleado</label>
                    </div>
 
                    <div class="input-field col s6">
                        <button type="submit" class="btn waves-effect waves-light" name="btnGuardar">Guardar
                            <i class="material-icons right">save</i>
                        </button>
                    </div>
    </form>
</div>
<h5><?php if($this->mensaje != null)
                    {echo $this->mensaje;}  ?>
</h5>

    <?php require 'view/static/footer.php'?>
    <script src="<?php echo constant('URL');?>resources/js/select.js"></script>
</body>
</html>