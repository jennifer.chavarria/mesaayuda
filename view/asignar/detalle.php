
 <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?php echo constant('URL');?>resources/css/textarea.css">
    <title>Detalle</title>
</head>
<body>
    <?php require 'view/static/header.php'?>

        <div class="section">
            <h4 class="center">Asignar Requisitos</h4>
            <div class="container">
                <div class="section"></div>
                <div class="divider"></div>
                

                <form  method="post" action="<?php echo constant('URL');?>asignar/editarDetalle" class="col s12">
                <div class="row">
                <div class="input-field col s6">
                            <textarea style="width: 738px; height: 253px;" name="txtReq" placeholder="Radicar Requisito: Mensaje" required></textarea>
                        </div>

                </div>
                    <div class="row">
                    <table width=100%>
                <thead>
                    <tr>
                        <th>asignar</th>
                        <th>id detalle</th>
                        <th>fecha</th>
                        <th>observacion</th>
                        <th>id empleado radicado</th>
                        <th>estado</th>
                    </tr>
                </thead>
                <tbody id="tbody-empleados">
                    <?php 
                        include_once 'model/detalleobject.php';
                        foreach($this->detalles as $registro){
                            $detalle = new DetalleObject();
                            $detalle = $registro;
                    ?> 
                    <tr id="trow-empleados-<?php echo $detalle->iddetallereq;?>">
                        <td>
                        <label>
                        <input name="txtSeleccion" type="radio" checked 
                        value="<?php echo $detalle->iddetallereq;?>"
                        />
                        <span>asignar</span>
                        </label>
                        </td>
                        <td><?php echo $detalle->iddetallereq;?></td>
                        <td><?php echo $detalle->fecha;?></td>
                        <td><?php echo $detalle->observacion;?></td>
                        <td><?php echo $detalle->fkemple;?></td>
                        <td><?php echo $detalle->fkestado;?></td>
                    </tr>       
                    <?php } ?>
                </tbody>
            </table>   

<!--FIN SECCION DE TABLA-->
                    </div>
                    <div class="row">
    
                        <div class="input-field col s5">
                        <select name="txtEmpleado" required>
                            <option value="" disabled selected>Asignar requisito</option>
                            <?php 
                                include_once 'model/empleadoobject.php';
                                foreach($this->empleados as $registro){
                                    $empleadoobject = new EmpleadoObject();
                                    $empleadoobject = $registro;
                            ?>
                                <option value="<?php echo $empleadoobject->idempleado;?>"><?php echo $empleadoobject->nombre;?></option>
                                <?php } ?>
                        </select>
                        <label>Empleado</label>
                        </div>
                    </div>
                    <div class="input-field col s3">
                            <button type="submit" class="btn waves-effect deep-orange accent-3" name="btnGuardar">Asignar
                            </button>
                        </div>
                    
                    
                    
                </form>
                
<h5><?php if($this->mensaje != null)
                    {echo $this->mensaje;}  ?>
</h5>
                
                
                
            </div> 
        </div>

       
    <?php require 'view/static/footer.php'?>
    <script src="<?php echo constant('URL');?>resources/js/util.js"></script>
    <script src="<?php echo constant('URL');?>resources/js/select.js"></script>