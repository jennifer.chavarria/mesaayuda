<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ingresar Empleado</title>
    
</head>
<body>
    <?php require 'view/static/header.php'?>
    <h5 class="center">Registro de Empleados</h5>
    <div class="container">

            <form class="col s12" method="post" action="<?php echo constant('URL');?>empleado/guardarEmpleado">

                <div class="row">
                    <div class="input-field col s6">
                        <i class="material-icons prefix">#</i>
                        <input id="first_name" type="number" class="validate" name="txtId">
                        <label for="first_name">ID Empleado</label>
                    </div>
                    <div class="input-field col s6">
                        <i class="material-icons prefix">account_circle</i>
                        <input id="icon_prefix" type="text" class="validate" name="txtNom">
                        <label for="icon_prefix">Nombre y Apellidos</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s6">
                        <i class="material-icons prefix">phone</i>
                        <input id="icon_telephone" type="text" class="validate" name="txtTel">
                        <label for="icon_telephone">Teléfono</label>
                    </div>
                    <div class="input-field col s6">
                        <i class="material-icons prefix">next_week</i>
                        <input id="icon_prefix1" type="text" class="validate" name="txtCargo">
                        <label for="icon_prefix1">Cargo</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s6">
                        <i class="material-icons prefix">email</i>
                        <input id="icon_email" type="text" class="validate" name="txtEmail">
                        <label for="icon_email">Email</label>
                    </div>
                    <div class="input-field col s6">
                        <select name="txtArea">
                            <option value="" disabled selected>Seleccione el Área del Empleado</option>
                            <?php 
                                include_once 'model/areaobject.php';
                                foreach($this->areas as $registro){
                                    $areasobject = new AreaObject();
                                    $areasobject = $registro;
                            ?>
                                <option value="<?php echo $areasobject->id;?>"><?php echo $areasobject->nombre;?></option>
                                <?php } ?>
                        </select>
                        <label>Área</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s6">
                        <button type="submit" class="btn waves-effect waves-light" name="btnGuardar">Guardar
                            <i class="material-icons right">save</i>
                        </button>
                    </div>
                    <div class="input-field col s6">
                            <a class ="btn waves-effect waves-light" href="<?php echo constant('URL');?>empleado/mostrarEmpleados">Volver a Tabla de Empleados</a>
                    </div>
                </div>
            </form>
            <h5><?php if($this->mensaje != null)
                    {echo $this->mensaje;}  ?>
            </h5>
    </div>
    


    <?php require 'view/static/footer.php' ?>
    <script src="<?php echo constant('URL');?>resources/js/select.js"></script>
</body>
</html>