    <div class="nav-container">
                    <ul id="slide-out" class="sidenav">
                        <li><div class="section"></div></li>
                        <li><div class="section"></div></li>
                        <li><a class="subheader">Seleccione una de las opciones</a></li>
                        <?php
                        if(!isset($_SESSION)) 
                        { 
                            session_start(); 
                        } 
                        
                        if($_SESSION["is_admin"]){
                        
                        ?>
                            <li><a id="adminUsuarios" href="#" style="color: #000000">
                                    <i class="material-icons">laptop</i>
                                    <p class="promo-caption">Administración de Usuarios</p>
                                </a>
                            </li>
                        <?php } ?>
                        
                        <?php 
                        if($_SESSION["is_jefe_area"] || $_SESSION["is_admin"]){
                        
                        ?>
                        <li><a id="gestionMaestros" href="<?php echo constant('URL');?>maestros" style="color: #000000">
                                <i class="material-icons">remove_red_eye</i>
                                <p class="promo-caption">Gestión de Maestros</p>
                            </a>
                        </li>
                        <li><a id="adminUsuarios" href="#" style="color: #000000">
                                <i class="material-icons">search</i>
                                <p class="promo-caption">Consultas e Informes</p>
                            </a>
                        </li>
                        <?php } ?>
                        
                        <?php 
                         if($_SESSION["is_jefe_area"] || $_SESSION["is_admin"]){
                        ?>
                         <li><a id="misRequisitosAsignar" href="<?php echo constant('URL');?>asignar" style="color: #000000">
                                <p class="promo-caption">Mis Requisitos</p>
                            </a>
                        </li>

                        <?php } else { ?>
                         <li><a id="misRequisitosResolver" href="<?php echo constant('URL');?>resolver" style="color: #000000">
                                <p class="promo-caption">Mis Requisitos</p>
                            </a>
                        </li>
                        <?php } ?>
                       
                        <li><a id="inicio" href="<?php echo constant('URL');?>main" style="color: #000000">
                                <p class="promo-caption">Inicio (Radicar un Requisito)</p>
                            </a>
                        </li>
                        <li><a id="cerrarSesion" href="<?php echo constant('URL');?>login/logout" style="color: #000000">
                                <p class="promo-caption">Cerrar Sesión</p>
                            </a>
                        </li>
                    </ul>
                    
    </div>  

                <div class="section"></div>
                <div class="section"></div>

                <div class="">
                    <a href="#" data-target="slide-out" class="sidenav-trigger place-right custom-color">Abrir Menú</a>
                </div>